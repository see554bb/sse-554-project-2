import javax.swing.*;

public class ScheduleCreator
{
	public static void main(String[] args)
	{
		JFrame frame = new Catalog();
		frame.setTitle("Course Catalog");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		JFrame table = new CatalogTable();
		table.setTitle("Course Catalog");
		table.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		table.setVisible(true);
	}
}