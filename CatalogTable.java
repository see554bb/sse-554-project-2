import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

public class CatalogTable extends JFrame
{
	private JTable table;
	static TableRowSorter<TableModel> sorter;
	static JMenuBar menuBar;
	static JMenu editMenu;
	static JMenuItem printSelectionItem;
	
	public CatalogTable()
	{
		Course a = new Course("BIO 111", 'F', 13, 14);
		Course b = new Course("EGR 126", 'M', 11, 14);
		Course c = new Course("CSC 205", 'W', 14, 17);
		Course d = new Course("CHR 210", 'M', 9, 13);
		Course e = new Course("MAT 225", 'T', 11, 12);
		Course f = new Course("ART 450", 'F', 8, 10);
		Course g = new Course("ECE 485", 'R', 11, 13);
		Course h = new Course("SSE 554", 'R', 17, 20);
	
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(a);
		courses.add(b);
		courses.add(c);
		courses.add(d);
		courses.add(e);
		courses.add(f);
		courses.add(g);
		courses.add(h);
	
		String[] columnNames = {"Name", "Day", "Start", "End"};
		Object[][] cells = new Object[courses.size()][columnNames.length];
		for(int i=0; i<courses.size(); i++)
		{
			cells[i][0] = courses.get(i).getName();
			cells[i][1] = courses.get(i).getDay();
			cells[i][2] = courses.get(i).getStart();
			cells[i][3] = courses.get(i).getEnd();
		}

		setSize(400, 500);

		TableModel model = new DefaultTableModel(cells, columnNames)
		{
			public Class<?> getColumnClass(int c)
			{
				return cells[0][c].getClass();
			}
		};
		
		table = new JTable(model);
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		sorter = new TableRowSorter<>(model);
		table.setRowSorter(sorter);
		sorter.setSortable(2, false);
		sorter.setSortable(3, false);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		editMenu = new JMenu("Edit");
		menuBar.add(editMenu);
		
		printSelectionItem = new JMenuItem("Print Selection");
		printSelectionItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				int[] selected = table.getSelectedRows();
				print(selected);
			}
		});
		editMenu.add(printSelectionItem);
	}
	
	public void print(int[] selected)
	{
		System.out.println("Your class schedule: \n");
		for(int i=0; i<selected.length; i++)
		{
			System.out.println(table.getModel().getValueAt(selected[i], 0) + " meets on " +
					table.getModel().getValueAt(selected[i], 1) + " from " +
					table.getModel().getValueAt(selected[i], 2) + " to " +
					table.getModel().getValueAt(selected[i], 3) + ".");
		}
	}
}