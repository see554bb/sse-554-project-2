import static org.junit.Assert.*;
import javax.swing.*;
import org.junit.Test;

public class CatalogTableTest
{
	@Test
	public void test()
	{
		JFrame table = new CatalogTable();
		table.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		table.setVisible(true);

		assertTrue(table.isDisplayable());
		
		assertFalse(CatalogTable.sorter.isSortable(2));
		
		assertTrue(CatalogTable.menuBar.isDisplayable());
		
		assertTrue(CatalogTable.editMenu.isMenuComponent(CatalogTable.printSelectionItem));
	}
}