
import javax.swing.tree.DefaultMutableTreeNode;

public class Course extends DefaultMutableTreeNode
{
	private String name;
	private char day;
	private int start;
	private int end;
	
	public Course(String name, char day, int start, int end)
	{
		this.name = name;
		this.day = day;
		//M-Monday, T-Tuesday, W-Wednesday, R-Thursday, F-Friday
		this.start = start;
		this.end = end;
	}
	
	public String getName()
	{
		return name;
	}
	
	public char getDay()
	{
		return day;
	}
	
	public int getStart()
	{
		return start;
	}
	
	public int getEnd()
	{
		return end;
	}
                @Override
        public String toString(){
            return this.name + " " + this.start + "-" + this.end;
        }
}
