
import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

class Catalog extends JFrame {

    static JPanel listPanel;
    static JList<Course> classList;
    static JLabel label;
    static JPanel buttonPanel;
    private ButtonGroup group;
    private String prefix = "Courses: ";

    public static JButton enrollButton;
    public static JButton dropButton;
    public DefaultTreeModel model;
    public static JTree tree;

    private DefaultMutableTreeNode[] DayNodes;

    public Catalog() {
        final int WIDTH = 875;
        final int HEIGHT = 400;

        setSize(WIDTH, HEIGHT);

        Course a = new Course("BIO 111", 'F', 1, 2);
        Course b = new Course("EGR 126", 'M', 11, 14);
        Course c = new Course("CSC 205", 'W', 14, 17);
        Course d = new Course("CHR 210", 'M', 9, 13);
        Course e = new Course("MAT 225", 'T', 11, 12);
        Course f = new Course("ART 450", 'F', 8, 10);
        Course g = new Course("ECE 485", 'R', 11, 13);
        Course h = new Course("SSE 554", 'R', 17, 20);

        Course[] classes = {a, b, c, d, e, f, g, h};

        classList = new JList<Course>(classes);
        classList.setVisibleRowCount(4);
        classList.setCellRenderer(new CourseCellRenderer());
        JScrollPane scrollPane = new JScrollPane(classList);

        listPanel = new JPanel();
        listPanel.add(scrollPane);

        classList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                StringBuilder text = new StringBuilder(prefix);
                for (Course value : classList.getSelectedValuesList()) {
                    text.append(value.getName());
                    text.append(" ");
                    text.append(value.getDay());
                    text.append(" ");
                    text.append(value.getStart());
                    text.append("-");
                    text.append(value.getEnd());
                    text.append("   ");
                }

                label.setText(text.toString());
            }
        });

        buttonPanel = new JPanel();
        group = new ButtonGroup();
        makeButton("Vertical", JList.VERTICAL);
        makeButton("Vertical Wrap", JList.VERTICAL_WRAP);
        makeButton("Horizontal Wrap", JList.HORIZONTAL_WRAP);

        //*****This is where I cut in*****
        JPanel ListPanel = new JPanel();
        GridLayout ListLayout = new GridLayout(3, 1);
        ListPanel.setLayout(ListLayout);
        label = new JLabel(prefix);

        ListPanel.add(listPanel);
        ListPanel.add(label);
        ListPanel.add(buttonPanel);

        JPanel TreePanel = new JPanel();
        GridLayout TreeLayout = new GridLayout(0, 1);
        TreePanel.setLayout(TreeLayout);

        model = new DefaultTreeModel(buildTreeWeek());
        tree = new JTree(model);
        tree.setRootVisible(false);
        JScrollPane TreeScrollPane = new JScrollPane(tree);

        TreePanel.add(makeEnrollButtons());
        TreePanel.add(TreeScrollPane);

        add(ListPanel, BorderLayout.NORTH);
        add(TreePanel, BorderLayout.CENTER);
    }

    private DefaultMutableTreeNode buildTreeWeek() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Week");

        String[] DaysOfTheWeek = new String[]{"Monday", "Tuesday",
            "Wednesday", "Thursday", "Friday"};
        DayNodes = new DefaultMutableTreeNode[DaysOfTheWeek.length];

        for (int i = 0; i <= DaysOfTheWeek.length - 1; ++i) {
            DayNodes[i] = new DefaultMutableTreeNode(DaysOfTheWeek[i]);
            root.add(DayNodes[i]);
        }
        return root;
    }

    private JPanel makeEnrollButtons() {
        JPanel panel = new JPanel();
        enrollButton = new JButton("Enroll");

        enrollButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                for (Course course : classList.getSelectedValuesList()) {
                    int DayIndex = 0;  //Default is Monday

                    switch (course.getDay()) {
                        case 'T':
                            DayIndex = 1;
                            break;
                        case 'W':
                            DayIndex = 2;
                            break;
                        case 'R':
                            DayIndex = 3;
                            break;
                        case 'F':
                            DayIndex = 4;
                            break;
                        default:
                            break;
                    }

                    boolean AlreadyEnrolled = false;
                    Enumeration children = DayNodes[DayIndex].children();

                    while (children.hasMoreElements()) {
                        if (children.nextElement().equals(course)) {
                            AlreadyEnrolled = true;
                        }
                    }

                    if (!AlreadyEnrolled) {
                        int selectedIndex
                                = DayNodes[DayIndex].getChildCount() - 1;
                        model.insertNodeInto(course, DayNodes[DayIndex], selectedIndex + 1);
                    }
                }
            }
        });
        panel.add(enrollButton);

        dropButton = new JButton("Drop");
        dropButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                DefaultMutableTreeNode selectedNode
                        = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

                if (selectedNode != null && selectedNode.getParent() != null) {
                    if (!selectedNode.getParent().toString().equalsIgnoreCase("Week")) {
                        model.removeNodeFromParent(selectedNode);
                    }
                }
            }
        });
        panel.add(dropButton);

        return panel;
    }

    private void makeButton(String name, final int orientation) {
        JRadioButton button = new JRadioButton(name);
        buttonPanel.add(button);
        if (group.getButtonCount() == 0) {
            button.setSelected(true);
        }
        group.add(button);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                classList.setLayoutOrientation(orientation);
                listPanel.revalidate();
            }
        });
    }
}

class CourseCellRenderer extends JLabel implements ListCellRenderer {

    public CourseCellRenderer() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
        Course course = (Course) value;
        setText(course.getName());
        if (isSelected) {
            setBackground(Color.blue);
            setForeground(Color.white);
        } else {
            setBackground(Color.white);
            setForeground(Color.black);
        }
        return this;
    }
}
