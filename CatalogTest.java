import static org.junit.Assert.*;
import javax.swing.JFrame;

import org.junit.Test;

public class CatalogTest
{
	@Test
	public void test()
	{
		JFrame frame = new Catalog();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		assertTrue(frame.isDisplayable());
		
		assertTrue(Catalog.listPanel.isDisplayable());
		
		assertNotNull(Catalog.label);
		
		assertTrue(Catalog.buttonPanel.isDisplayable());
		
		assertTrue(Catalog.label.isDisplayable());
                
                assertTrue(Catalog.tree.isDisplayable());
                assertTrue(Catalog.enrollButton.isDisplayable());
                assertTrue(Catalog.dropButton.isDisplayable());


	}
        
    @Test  //Make sure tree is populated with days of the week
    public final void TreeIsPopulated() {
        Catalog Test = new Catalog();
        //Make sure there are 5 days of the week
        assertEquals(5, Test.model.getChildCount(Test.model.getRoot()));

        //Make sure the root node is defined as Week
        assertEquals("Week", Test.model.getRoot().toString());

        //Just check the first child to ensure it is the first day of the week
        assertEquals("Monday",
                Test.model.getChild(Test.model.getRoot(), 0).toString());
    }
    
    @Test  //Ensure the enroll button works
    public final void EnrollButtonWorks() {
        Catalog Test = new Catalog();

        //The first object should be Monday and it should have 0 children
        Object FirstDay = Test.model.getChild(Test.model.getRoot(), 0);
        assertEquals(0, Test.model.getChildCount(FirstDay));

        //After selecting a Monday class and enrolling, Monday should have 1 child
        Catalog.classList.setSelectedIndex(1);
        Test.enrollButton.doClick();
        assertEquals(1, Test.model.getChildCount(FirstDay));
    }

    @Test  //ensure the drop button works
    public final void DropButtonWorks() {
        Catalog Test = new Catalog();
        Object FirstDay = Test.model.getChild(Test.model.getRoot(), 0);
        Catalog.classList.setSelectedIndex(1);
        Test.enrollButton.doClick();

        //The first object should be Monday and it should have 1 child
        assertEquals(1, Test.model.getChildCount(FirstDay));

        //Expand the first visible row, Monday...
        Test.tree.expandRow(0);
        //and select the class that should have been added to it (next row down)
        Test.tree.setSelectionRow(1);
        Test.dropButton.doClick();
        assertEquals(0, Test.model.getChildCount(FirstDay));
    }
}